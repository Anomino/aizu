__Author__ = 'Anomino'
# coding:UTF-8


def AskUser(number) : 
    player_A, player_B = [], []
    for value in range(0, number) : 
        answer = raw_input()
        answer = answer.split( )
        player_A.append(int(answer[0]))
        player_B.append(int(answer[1]))

    return player_A, player_B
    
def CalcScore(player_A, player_B) : 
    score_A, score_B = 0, 0

    for value in range(0, len(player_A)) : 
        if player_A[value] < player_B[value] : 
            score_B = score_B + player_A[value] + player_B[value]
        elif player_A[value] > player_B[value] : 
            score_A = score_A + player_A[value] + player_B[value]
        else : 
            score_A = score_A + player_A[value]
            score_B = score_B + player_B[value]

    return score_A, score_B

if __name__ == '__main__' : 
    total_A, total_B = [], []
    number = int(raw_input())

    while number != 0 :
        player_A, player_B = AskUser(number)
        score_A, score_B = CalcScore(player_A, player_B)
        total_A.append(score_A)
        total_B.append(score_B)
        number = int(raw_input())

    for value in range(0, len(total_A)) : 
        print total_A[value], total_B[value]
