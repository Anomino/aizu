__Author__ = 'Anomino'
#coding:UTF-8
import numpy as np

def AskUser() : 
		number = int(raw_input())
		baum = []

		for value in range(number) : 
			baum.append(int(raw_input()))

		return baum

def CutBaum(baum, cuts) :
		ideal = sum(baum) / cuts
		piece_of_baum = []
		tmp_baum = 0

		for value in range(len(baum)) : 
				if len(baum) == (cuts - 1) : 
						piece_of_baum.append(sum(baum[value :]))
						break

				if np.abs(ideal - tmp_baum) > np.abs(ideal - tmp_baum - baum[value]) : 
						tmp_baum = tmp_baum + baum[value]
				else : 
						piece_of_baum.append(tmp_baum)
						tmp_baum = baum[value]
		return min(piece_of_baum)

if __name__ == '__main__' : 
		baum = AskUser()
		min_baum = CutBaum(baum, 3)

		print min_baum
